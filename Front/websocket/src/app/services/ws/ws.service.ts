import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WsService {

  webSocketEndPoint = `ws://127.0.0.1:8080/ws`;
  webSocket: WebSocket;
  clientId = new BehaviorSubject<string>(null);

  constructor() { }

  get getClientId() {
    return this.clientId.asObservable();
  }

  public openWebSocket(){
    this.webSocket = new WebSocket(`${this.webSocketEndPoint}`);

    this.webSocket.onopen = (event) => {
      console.log('Open: ', event);
    };

    this.webSocket.onmessage = (event) => {
      // const chatMessageDto = JSON.parse(event.data);
      // this.chatMessages.push(chatMessageDto);
      this.onMessageReceived(event.data);
    };

    this.webSocket.onclose = (event) => {
      console.log('Close: ', event);
    };
  }

  public sendMessage(chatId: string){
    this.webSocket.send(chatId);
  }

  public closeWebSocket() {
    this.webSocket.close();
  }

  private onMessageReceived(message) {
    this.changeClientId(message);
}

  private changeClientId = (clientId: string) => {
    this.clientId.next(clientId);
  };

}
