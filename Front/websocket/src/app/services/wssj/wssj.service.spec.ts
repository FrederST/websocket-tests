import { TestBed } from '@angular/core/testing';

import { WssjService } from './wssj.service';

describe('WssjService', () => {
  let service: WssjService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WssjService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
