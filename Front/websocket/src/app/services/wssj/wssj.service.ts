import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';

@Injectable({
  providedIn: 'root'
})
export class WssjService {

  webSocketEndPointDesktop= `http://127.0.0.1:8080/ws`;
  topic = '/chat';
  stompClient: any;
  clientId = new BehaviorSubject<string>(null);

  constructor() {
  }

  get getClientId() {
    return this.clientId.asObservable();
  }

  connect = () => {
      const ws = new SockJS(this.webSocketEndPointDesktop);
      this.stompClient = Stomp.over(ws);
      this.stompClient.connect({}, () => {
        this.stompClient.subscribe(this.topic, (data) => {
          this.onMessageReceived(data);
        });
      }, this.errorCallBack);
  };

  disconnect = () => {
      if (this.stompClient !== null) {
          this.stompClient.disconnect();
      }
      // console.log('Disconnected');
  };

  errorCallBack = (error) => {
      // console.log('errorCallBack -> ' + error);
      setTimeout(() => {
          this.connect();
      }, 5000);
  };


  onMessageReceived(message) {
      console.log('Message Recieved from Server :: ' + message);
      this.changeClientId(message?.body);
      // console.log(message);
  }

  changeClientId = (clientId: string) => {
    this.clientId.next(clientId);
  };
}
