import { Component, OnInit } from '@angular/core';
import { WsService } from '../services/ws/ws.service';
import { WssjService } from '../services/wssj/wssj.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private ws: WsService, private wsjs: WssjService) {}

  ngOnInit(): void {
    //this.ws.openWebSocket();
    this.wsjs.connect();
  }

}
