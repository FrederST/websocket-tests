package com.tests.websocket.config;

import com.quileia.securitylibrary.config.WebSecurityConfig;
import com.quileia.securitylibrary.jwt.JwtFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true,
                            jsr250Enabled = true,
                            prePostEnabled = true)
public class WebSecurityConfigEz extends WebSecurityConfig {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        this.allMatches.add("/chat");
        http
                .cors().and()
                .csrf().disable()
                .addFilterBefore(new JwtFilter(), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(allMatches.toArray(String[]::new)).permitAll()
                .antMatchers(HttpMethod.GET, getMatches.toArray(String[]::new)).permitAll()
                .anyRequest().authenticated();
    }
}
